import {Row, Col} from 'react-bootstrap'; 

export default function Banner({bannerData}) {
	return(
		<Row className="p-5" >
			<Col>
				<h1> {bannerData.title} </h1>
				<p className="my-4"> {bannerData.content} </p>
				<a className="btn btn-primary" href="/">INSERT ACTION HERE</a>	
			</Col>
		</Row>
	);
}

//expose the component
