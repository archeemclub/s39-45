import {Row, Col, Container, Card} from 'react-bootstrap';

export default function Banner(){
	return(
			<Row className="p-3 my-3 mb-10">
				<Col>
				<h6 className="p-3 my-3 mb-3"> About Me </h6>
				<h4 className="p-3 my-3 mb-10"> Archee Lee </h4>
				<h6 className="p-3 my-3mb-10">I'm an Aspiring Full Stack Web Developer </h6>
				<p className="p-3 my-3mb-10"> I'm currently working as a Operations Manager
				in a BPO company but wants to pursue my Web Developer Career. Im a Career Shifter and wants
				to explore more in this industry. There's nothing More to share at the moment.</p>

				<Card>
			<Card.Body className="p-3 my-3mb-10">
				<Card.Title>
				Contact Me:
				</Card.Title>
				<Card.Text>
				Email Address: archeemclub@gmail.com
				</Card.Text>
				<Card.Text>
				Mobile No: +63 906 563 5420
				</Card.Text>
	 			<Card.Text>
				Address: Raxabago, Tondo, Manila.
				</Card.Text>
			</Card.Body>
		</Card>
				</Col>
			</Row>
		);
}

