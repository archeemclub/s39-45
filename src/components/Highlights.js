import {Row, Col, Card, Container} from 'react-bootstrap'


export default function Highlights() {
	return(
		<Container>
			<Row className="my-3">
		   {/*1st Hightlight*/}
		   <Col xs={12} md={4}>
			  <Card className="p-4 cardHighlight">
			     <Card.Body>
			     	<Card.Title> Learn From Magic </Card.Title>
			     	<Card.Text>
			     		Want to learn Witcher Magic like, Aard, Quen, Sign and many more? Enroll Now!	
			     	</Card.Text>
			     </Card.Body>
			  </Card>		      
		   </Col>

		   {/*2nd Hightlight*/}
		   <Col xs={12} md={4}>
			  <Card className="p-4 cardHighlight">
			     <Card.Body>
			     	<Card.Title> Learn Combat Skill </Card.Title>
			     	<Card.Text>
			     		Want to learn hand-to-hand combat and survival skills? Enroll Now!	
			     	</Card.Text>
			     </Card.Body>
			  </Card>		      
		   </Col>

			{/*3rd Hightlight*/}
			<Col xs={12} md={4}>
				 <Card className="p-4 cardHighlight">
				     <Card.Body>
				     	<Card.Title> Learn Sword Skills</Card.Title>
				     	<Card.Text>
				     		Want to learn Swordmanship Witcher Style at its finest? Enroll Now!
				     	</Card.Text>
				     </Card.Body>
				  </Card>		      
			</Col>
			</Row>
		</Container>		
	)
}
