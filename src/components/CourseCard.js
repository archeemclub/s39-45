import {Card} from 'react-bootstrap';

export default function CourseCard(){
	return(
		<Card>
			<Card.Body>
				<Card.Title>
				Swordsmanship
				</Card.Title>
				<Card.Text>
				Steel and Silver Sword
				</Card.Text>
				<Card.Text>
				150 Coin for 1 Year
				</Card.Text>
				<a href="/" className="btn btn-primary">
				View Course
				</a>
			</Card.Body>
		</Card>
	);
};