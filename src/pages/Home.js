import Banner from './../components/Banner'
import Highlights from './../components/Highlights'


const data = {
  title: 'Welcome to the Kaer Morhen Witcher School',
  content: 'Opportunities for the Chosen 1'
}


export default function Home() {
   return(
   	<div>
	   	 <Banner bannerData={data}/>
	   	 <Highlights />
   	</div>
   );
};
