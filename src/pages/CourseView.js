import { useState, useEffect } from 'react';
import Hero from './../components/Banner';
import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useParams } from 'react-router-dom'; 

const data = {
	title: 'Witcher Magic',
	content: 'You will learn the secret magic and potion creations used by Witcher'
}

export default function CourseView(){

	//state of our course details
	const [courseInfo, setCourseInfo] = useState({
		name: null,
		description: null,
		price: null
	}); 

	console.log(useParams()); 
	const {id} = useParams()
	console.log(id); 

	
    useEffect(() => {

    	
    	fetch(`https://enigmatic-tundra-57512.herokuapp.com/courses/all${id}`).then(res => res.json())
    	.then(convertedData => {
 
    		setCourseInfo({
    			name:  convertedData.name,
    			description: convertedData.description,
    			price: convertedData.price
    		}) 
    	}); 
    },[id])

	
    const enroll = () => {
    	return(
    		Swal.fire({
    		    icon: "success",
				title: 'You survived the Trial of Grass',
				text: 'Long Lived Witcher'
    		})
    	);
    }; 

	return(
	  <>
		<Hero bannerData={data} />
		<Row>
		   <Col>
		      <Container>
			      <Card className="text-center">
			         <Card.Body>
			            {/*<!-- Insert Comment Here --> */}
			            {/* Course Name */}
			         	<Card.Title>
			         		<h2> {courseInfo.name} </h2>
			         	</Card.Title>
			         	{/*  Course Description */}
			         	<Card.Subtitle>
			         		<h6 className="my-4"> Description: Magic and Alchemy </h6>
			         	</Card.Subtitle>
			         	<Card.Text>
			         		{courseInfo.description}
			         	</Card.Text>
			         	{/*  Course Price */}
			         	<Card.Subtitle>
			         		<h6 className="my-4"> Price: 1000 coin</h6>
			         	</Card.Subtitle>
			         	<Card.Text>
			         		PHP: 100,000 {courseInfo.price}
			         	</Card.Text>
			         </Card.Body>

			         <Button variant="warning" className="btn-block" onClick={enroll}> 
			            Enroll
			         </Button>

			         <Link className="btn btn-success btn-block mb-5" to="/login">
			         	Login to Enroll
			         </Link>
			      </Card>
		      </Container>
		   </Col>
		</Row>
	  </>
	);
}; 
