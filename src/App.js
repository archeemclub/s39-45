import { useState, useEffect } from 'react'; 
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import LoginPage from './pages/Login';
import Catalog from './pages/Courses';
import ErrorPage from './pages/Error'; 
import CourseView from './pages/CourseView';
import Logout from './pages/Logout'; 
import { UserProvider } from './UserContext'; 
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import './App.css';

function App() {

  const [user, setUser] = useState({
     id: null, 
     isAdmin: null     
  }); 
  const unsetUser = () => {
      localStorage.clear(); 
      setUser({
         id: null,
         isAdmin: null
      }); 
  }
  

  useEffect(() => {

      let token = localStorage.getItem('accessToken');

      fetch('https://whispering-spire-20350.herokuapp.com/users/details', {
         headers: {
            Authorization: `Bearer ${token}`
         }
      })
      .then(res => res.json())
      .then(convertedData => {

         if (typeof convertedData._id !== "undefined") {
           setUser({
              id: convertedData._id, 
              isAdmin: convertedData.isAdmin
           });
         } else {
            setUser({
              id: null, 
              isAdmin: null
           });
         }
      }); 

  },[user]);


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
         <AppNavBar />
         <Routes>
            <Route path='/' element={<Home />}  /> 
            <Route path='/register' element={<Register />}  />
            <Route path='/courses' element={<Catalog />}  />
            <Route path='/login' element={<LoginPage />} />
            <Route path='/courses/view/:id' element={<CourseView />}  />
            <Route path='/logout' element={<Logout />}  />
            <Route path='*' element={<ErrorPage />} />
         </Routes> 
      </Router>  
    </UserProvider>
  );
}

export default App;
